package com.acme.api.order.entity;

import com.acme.api.order.enums.OrderStatusEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    private List<Item> items;
    private Order order;

    @BeforeEach
    void setUp() {
        items = new ArrayList<>();
        items.add(
                new Item("id-item-1", "First Item to be Tested", BigDecimal.TEN, 5)
        );
        items.add(
                new Item("id-item-2", "2 Item Test", BigDecimal.ONE, 2)
        );
        items.add(
                new Item("id-item-3", "3 Item Test", BigDecimal.valueOf(125.5), 1)
        );

        order = new Order();
        order.setId("order-test");
        order.setAddress("The blue hole of the Moon.");
        order.setConfirmationDate(LocalDate.now());
        order.setStatus(OrderStatusEnum.RECEIVED);
        order.setItems(items);
    }

    @Test
    void testOrderEntity() {
        assertNotEquals(null, order);
        assertEquals(order.getId(), "order-test");
        assertEquals(order.getAddress(), "The blue hole of the Moon.");
        assertEquals(order.getConfirmationDate(), LocalDate.now());
        assertEquals(order.getStatus(), OrderStatusEnum.RECEIVED);
        assertEquals(order.getItems(), items);
    }

    @Test
    void equals() {
        assertFalse(order.equals(null));

        assertFalse(order.equals(
                new Order("test", "test address", LocalDate.now(), OrderStatusEnum.REFUSED, items)
        ));

        assertTrue(order.equals(order));
    }

    @Test
    void hashCodes() {
        assertNotEquals(BigDecimal.ONE, order.hashCode());
    }
}