package com.acme.api.exception.handler;

import com.acme.api.exception.DomainBusinessException;
import com.acme.api.exception.InternalServerException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

class ApiErrorTest {

    private static final String DEFAULT_ERROR = "Fail to make coffee, an teapot founded!";
    private static final String DEFAULT_ERROR_MESSAGE = "A teapot founded while make some coffee!";
    private ApiError apiError;

    @BeforeEach
    void setUp() {
        apiError = new ApiError(HttpStatus.I_AM_A_TEAPOT.value(),
                DEFAULT_ERROR, DEFAULT_ERROR_MESSAGE);
    }

    @Test
    void getStatusCode() {
        assertNotNull(apiError);

        assertNotEquals(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value(), apiError.getStatusCode());
        assertEquals(HttpStatus.I_AM_A_TEAPOT.value(), (int) apiError.getStatusCode());
    }

    @Test
    void setStatusCode() {
        assertNotNull(apiError);

        apiError.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());

        assertNotEquals(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value(), apiError.getStatusCode());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), (int) apiError.getStatusCode());
    }

    @Test
    void getError() {
        assertNotNull(apiError);

        assertNotEquals("empty", apiError.getError());
        assertEquals(DEFAULT_ERROR, apiError.getError());
    }

    @Test
    void setError() {
        assertNotNull(apiError);

        apiError.setError("Empty Error");

        assertNotEquals(DEFAULT_ERROR, apiError.getError());
        assertEquals("Empty Error", apiError.getError());
    }

    @Test
    void getMessage() {
        assertNotNull(apiError);

        assertNotEquals("", apiError.getMessage());
        assertEquals(DEFAULT_ERROR_MESSAGE, apiError.getMessage());
    }

    @Test
    void setMessage() {
        assertNotNull(apiError);

        final String newMessage = new DomainBusinessException().getMessage();
        apiError.setMessage(newMessage);

        assertNotEquals(DEFAULT_ERROR_MESSAGE, apiError.getMessage());
        assertEquals(newMessage, apiError.getMessage());
    }

    @Test
    void fromHttpError() {
        assertNotNull(apiError);

        final ApiError httpError =
                ApiError.fromHttpError(HttpStatus.INTERNAL_SERVER_ERROR, new InternalServerException());

        assertNotNull(httpError);
        assertNotEquals(apiError.getError(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        assertEquals(new InternalServerException().getMessage(), httpError.getMessage());
    }
}