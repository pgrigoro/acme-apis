package com.acme.api.exception.handler;

import com.acme.api.exception.DomainBusinessException;
import com.acme.api.exception.InternalServerException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

class HttpErrorExceptionHandlerTest {

    private HttpErrorExceptionHandler httpErrorExceptionHandler;

    @BeforeEach
    void setUp() {
        httpErrorExceptionHandler = new HttpErrorExceptionHandler();
    }

    @Test
    void domainBusinessException() {
        assertNotNull(httpErrorExceptionHandler);

        final ApiError apiError =
                httpErrorExceptionHandler.domainBusinessException(new DomainBusinessException());

        assertNotNull(apiError);
        assertNotEquals("", apiError.getMessage());
        assertEquals((int) apiError.getStatusCode(), HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    @Test
    void internalServerException() {
        assertNotNull(httpErrorExceptionHandler);

        final ApiError apiError =
                httpErrorExceptionHandler.internalServerException(new InternalServerException());

        assertNotNull(apiError);
        assertNotEquals("", apiError.getMessage());
        assertEquals((int) apiError.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
}