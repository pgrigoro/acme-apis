package com.acme.api.store.entity;

import com.acme.StoreApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ActiveProfiles("test")
@ContextConfiguration(classes = {StoreApplication.class})
class StoreTest {

    @BeforeEach
    void setUp() {

    }

    @Test
    void getName() {

    }

    @Test
    void setName() {
    }

    @Test
    void getAddress() {
    }

    @Test
    void setAddress() {
    }

    @Test
    void getId() {
    }

    @Test
    void setId() {
    }

    @Test
    void equals() {
    }

    @Test
    void hashCodes() {
    }
}