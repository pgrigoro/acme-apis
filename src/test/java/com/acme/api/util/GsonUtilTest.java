package com.acme.api.util;

import com.acme.api.order.entity.Order;
import com.acme.api.order.enums.OrderStatusEnum;
import com.google.gson.Gson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GsonUtilTest {

    private GsonUtil gsonUtil;

    @BeforeEach
    void setUp() {
        Gson gson = new Gson();
        gsonUtil = new GsonUtil(gson);
    }

    @Test
    void getSimpleStringJsonObject() {

        assertNotNull(gsonUtil);

        final List<Order> orderList = getOrders();
        final String simpleStringJsonObject = gsonUtil.getSimpleStringJsonObject(orderList);

        assertNotNull(simpleStringJsonObject);
        assertTrue(simpleStringJsonObject.contains("{"));
        assertTrue(simpleStringJsonObject.contains("}"));
    }

    @Test
    void getPrettyStringJsonObject() {

        assertNotNull(gsonUtil);

        final List<Order> orderList = getOrders();
        final String simpleStringJsonObject = gsonUtil.getPrettyStringJsonObject(orderList);

        assertNotNull(simpleStringJsonObject);
        assertTrue(simpleStringJsonObject.contains("{"));
        assertTrue(simpleStringJsonObject.contains("["));
        assertTrue(simpleStringJsonObject.contains(" "));
        assertTrue(simpleStringJsonObject.contains("]"));
        assertTrue(simpleStringJsonObject.contains("}"));
    }

    private List<Order> getOrders() {

        List<Order> orderList = new ArrayList<>();

        Order orderUnitTests = new Order();
        orderUnitTests.setId("order-unit-tests");
        orderUnitTests.setAddress("Description from order one.");
        orderUnitTests.setStatus(OrderStatusEnum.RECEIVED);
        orderUnitTests.setConfirmationDate(LocalDate.now());
        orderUnitTests.setItems(new ArrayList<>());

        orderList.add(orderUnitTests);
        return orderList;
    }

}