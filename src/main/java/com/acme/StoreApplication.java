package com.acme;

import com.acme.api.config.WebConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication(scanBasePackages = {"com.acme"})
@ComponentScan(value = "com.acme", lazyInit = true)
@EnableMongoRepositories
@EnableAutoConfiguration
public class StoreApplication {

	@Bean
	public WebMvcConfigurer corsConfigurer(){
		return new WebConfig();
	}

	public static void main(String[] args) {
		SpringApplication.run(StoreApplication.class, args);
	}
}
