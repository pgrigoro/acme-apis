package com.acme.api.order.service;

import com.acme.api.exception.DomainBusinessException;
import com.acme.api.order.dto.OrderDTO;
import com.acme.api.order.entity.Order;
import com.acme.api.order.enums.OrderStatusEnum;
import com.acme.api.order.repository.OrderRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void createNewOrder(final @NotNull @Valid OrderDTO orderDTO) {

        final Optional<Order> orderOptional = orderRepository.findById(orderDTO.getId());

        if (orderOptional.isEmpty()) {

            final Order newOrder =
                    new Order(orderDTO.getId(), orderDTO.getAddress(),
                            orderDTO.getConfirmationDate(), orderDTO.getStatus(), orderDTO.getItems()
                    );

            orderRepository.saveOrder(newOrder);
        } else {
            throw new DomainBusinessException("Sorry, but another Order have the same ID. Consider change the ID and try again.");
        }
    }

    public Optional<Order> getStoreById(@NotNull @NotBlank final String orderId) {
        return orderRepository.findById(orderId);
    }

    public Optional<Iterable<Order>> getAllStore() {

        final Iterable<Order> stores = orderRepository.findAll();

        return CollectionUtils.sizeIsEmpty(stores)
                ? Optional.empty()
                : Optional.of(stores);
    }

    public void refundOrder(@NotNull @NotBlank final String orderId) {

        final Optional<Order> orderOptional = orderRepository.findById(orderId);

        if (orderOptional.isPresent()) {

            final Order orderToRefund = orderOptional.get();

            if (this.orderCanBeRefunded(orderToRefund)) {

                orderToRefund.setStatus(OrderStatusEnum.REFUNDED);
                orderRepository.saveOrder(orderToRefund);

            } else {
                throw new DomainBusinessException("Sorry! But the selected Order can't be refunded. Check it with customer support.");
            }
        } else {
            throw new DomainBusinessException();
        }
    }

    private boolean orderCanBeRefunded(@NotNull final Order orderToRefund) {

        final boolean isOrderConfirmed = orderToRefund.getStatus() == OrderStatusEnum.CONFIRMED;

        //TODO Implement code to verify payment status from Payment API.
        final boolean isPaymentConcluded = Boolean.TRUE;

        final boolean isUntilTenDays = DAYS.between(orderToRefund.getConfirmationDate(), LocalDate.now()) <= 10;

        return isOrderConfirmed && isPaymentConcluded && isUntilTenDays;
    }

}
