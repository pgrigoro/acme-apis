package com.acme.api.store.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

public class StoreDTO implements Serializable {

    private static final long serialVersionUID = 1750602087874856757L;

    @NotNull
    @NotBlank
    private String id;

    @NotNull
    @NotBlank
    private String name;

    @NotNull
    @NotBlank
    private String address;

    public StoreDTO() {
        //Default constructor for jackson
    }

    public StoreDTO(@NotNull @NotBlank String id, @NotNull @NotBlank String name,
                    @NotNull @NotBlank String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StoreDTO storeDTO = (StoreDTO) o;
        return getId().equals(storeDTO.getId()) &&
                getName().equals(storeDTO.getName()) &&
                getAddress().equals(storeDTO.getAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getAddress());
    }
}
