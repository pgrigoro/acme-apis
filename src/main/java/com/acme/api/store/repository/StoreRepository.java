package com.acme.api.store.repository;

import com.acme.api.store.entity.Store;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface StoreRepository extends MongoRepository<Store, String> {

    /**
     * To use void and @NotNull annotation.
     * @param store Store object
     */
    default void saveStore(@NotNull Store store){
        save(store);
    }

    @Override
    Optional<Store> findById(@NotNull @NotBlank String storeId);
}
