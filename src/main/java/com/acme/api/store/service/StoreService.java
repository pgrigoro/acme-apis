package com.acme.api.store.service;

import com.acme.api.exception.DomainBusinessException;
import com.acme.api.store.dto.StoreDTO;
import com.acme.api.store.entity.Store;
import com.acme.api.store.repository.StoreRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Service
public class StoreService {

    private final StoreRepository storeRepository;

    public StoreService(StoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    public Optional<Store> getStoreById(@NotNull @NotBlank final String storeId) {
        return storeRepository.findById(storeId);
    }

    public void createNewStore(@NotNull @Valid final StoreDTO storeDTO) {

        final Optional<Store> optionalStore = this.storeRepository.findById(storeDTO.getId());

        if (optionalStore.isEmpty()) {

            final Store newStore = new Store(storeDTO.getId(), storeDTO.getName(), storeDTO.getAddress());

            storeRepository.saveStore(newStore);

        } else {
            throw new DomainBusinessException("Sorry, but another Store have the same ID. Consider change the ID and try again.");
        }
    }

    public void updateStoreInformation(@NotNull @NotBlank final String storeId, @NotNull @Valid final StoreDTO storeDTO) {

        final Optional<Store> optionalStore = storeRepository.findById(storeId);

        if (optionalStore.isPresent()) {

            final Store storeToBeUpdated = optionalStore.get();

            //For security the Store ID are not updated.
            storeToBeUpdated.setName(storeDTO.getName());
            storeToBeUpdated.setAddress(storeDTO.getAddress());

            storeRepository.saveStore(storeToBeUpdated);

        } else {
            throw new DomainBusinessException();
        }
    }

    public Optional<Iterable<Store>> getAllStore() {

        final Iterable<Store> stores = storeRepository.findAll();

        return CollectionUtils.sizeIsEmpty(stores)
                ? Optional.empty()
                : Optional.of(stores);
    }

}
