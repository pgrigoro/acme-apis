package com.acme.api.exception;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class DataBaseException extends RuntimeException {

    private static final long serialVersionUID = 8338299999763344091L;
    private static final String DEFAULT_ERROR_MESSAGE =
            "Sorry! We're having trouble with our database. Please try again later.";

    public DataBaseException(@NotNull @NotBlank final String message) {
        super(message);
    }

    public DataBaseException() {
        super(DEFAULT_ERROR_MESSAGE);
    }
}
