package com.acme.api.exception.handler;

import com.acme.api.exception.DomainBusinessException;
import org.springframework.http.HttpStatus;

public class ApiError {

    private Integer statusCode;
    private String error;
    private String message;

    public ApiError() {
    }

    public ApiError(Integer statusCode, String error, String message) {
        this.statusCode = statusCode;
        this.error = error;
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    static ApiError fromHttpError(HttpStatus httpStatus, Exception exception) {

        if (exception instanceof DomainBusinessException) {

            DomainBusinessException domainBusnessException = (DomainBusinessException) exception;

            return new ApiError(
                    httpStatus.value(), httpStatus.getReasonPhrase(), domainBusnessException.getMessage()
            );

        } else {
            return new ApiError(httpStatus.value(), httpStatus.getReasonPhrase(), exception.getMessage());
        }
    }

}
