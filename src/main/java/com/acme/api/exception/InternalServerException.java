package com.acme.api.exception;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class InternalServerException extends RuntimeException {

    private static final long serialVersionUID = 7954955070944057599L;
    private static final String DEFAULT_ERROR_MESSAGE =
            "Sorry! We're having trouble processing your request at the moment. Please try again later.";

    public InternalServerException(@NotNull @NotBlank final String message) {
        super(message);
    }

    public InternalServerException() {
        super(DEFAULT_ERROR_MESSAGE);
    }
}
